# Contributing to CarNetCsvFileGeneration

Disclaimer: I am developing this in my free time. Please give me some time to answer.

Things where you can help:

- Translations
- Refactoring the code to a more readable state.

If you have a Gitlab Account (the site you are currently seeing) please use Issues and Merge Requests for Contributions.  If you would like to contribute without having an account then please get in touch via the GSAK-Forums or via [matrixfueller@gmail.com](mailto:matrixfueller@gmail.com).

