# CarNetCsvFileGeneration

Release on gask.net: [GSAK Forums](https://gsak.net/board/index.php?act=ST&f=29&t=33898)

Support Forum on gask.net: [GSAK Forums](https://gsak.net/board/index.php?act=ST&f=7&t=33899)

This macro generates `.csv` files. I began developing this because the [VW-Car-Net](https://www.volkswagen-carnet.com/de_de.html) has serveral limitations:

- It allows you only to upload 5MB and if you have large Pocket Querys you easily reach that limit.
- It has only a very limited space to display information onboard your car.

The macro is working the following way:

1. Start the macro.
2. Choose a Word (or multiple) with which your filters are choosen for you.
3. If you whish you can change the language of the macro interface (and error messages).
4. Hit run
5. After some time the files should be written to your disk.

The macro is displaying a status window while running. The filenames are `Filtername.csv`.

If you wish to contribute to the development of the macro please referr to `CONTRIBUTING.md`.